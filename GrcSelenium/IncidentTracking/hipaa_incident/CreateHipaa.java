package hipaa_incident;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class CreateHipaa {

	
	WebDriver driver;
	ConfigFileReader cfr;
	
	public CreateHipaa(WebDriver driver){
		this.driver=driver;
	}
	
	public void hipaa_form() throws InterruptedException  {
	      driver.findElement(By.cssSelector(".fa-folder-o")).click();
	      WebDriverWait  block = new WebDriverWait(driver,10);
		    WebElement indept = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='1171-label']")));
		    indept.click();
		    driver.findElement(By.xpath("//span[contains(.,\'OK\')]")).click();
		  driver.findElement(By.id("Title")).click();
		  driver.findElement(By.id("Title")).sendKeys("title");
		  driver.findElement(By.id("ContactNo")).click();
		  driver.findElement(By.id("ContactNo")).sendKeys("contact");
		  driver.findElement(By.id("LocationDetail")).click();
		  driver.findElement(By.id("LocationDetail")).sendKeys("location");
		  driver.findElement(By.id("System")).click();
		  driver.findElement(By.id("System")).sendKeys("sysytem");
	      driver.findElement(By.cssSelector("#ImpactCategory_chosen span")).click();
	      driver.findElement(By.cssSelector("#Severity_chosen span")).click();
		  driver.findElement(By.id("IncidentDesc")).click();
		  driver.findElement(By.id("IncidentDesc")).sendKeys("incident");
		  driver.findElement(By.id("OtherInvolved")).click();
		  driver.findElement(By.id("OtherInvolved")).sendKeys("other");
		  driver.findElement(By.id("IsSecurityIncidentResponseTeam")).click();
		  driver.findElement(By.id("IdentificationMeasures")).click();
		  driver.findElement(By.id("IdentificationMeasures")).sendKeys("identification");
		  driver.findElement(By.id("RecoveryMeasures")).click();
		  driver.findElement(By.id("RecoveryMeasures")).sendKeys("recovery");
		  driver.findElement(By.id("EvidenceCollected")).click();
		  driver.findElement(By.id("EvidenceCollected")).sendKeys("evidence");
		  driver.findElement(By.id("Recommendation")).click();
		  driver.findElement(By.id("Recommendation")).sendKeys("recommendation");
		  driver.findElement(By.id("InitialReportCompletedBy")).click();
		  driver.findElement(By.id("InitialReportCompletedBy")).sendKeys("initial");
		  driver.findElement(By.id("FollowUpCompletedBy")).click();
		  driver.findElement(By.id("FollowUpCompletedBy")).sendKeys("follow");
		  driver.findElement(By.cssSelector("#IncidentStatus_chosen span")).click();
		  //driver.findElement(By.cssSelector("#IncidentStatus_chosen .active-result:nth-child(1)")).click();
//		  driver.findElement(By.cssSelector(".chosen-default > span")).click();
//		  driver.findElement(By.cssSelector("#Priority_chosen .active-result:nth-child(1)")).click();
		  driver.findElement(By.cssSelector(".btn-info")).click();
		    Thread.sleep(3000);
	    }
}
