package grclogout;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import dataprovider.ConfigFileReader;

public class GrcLogout {

	
	WebDriver driver;
	ConfigFileReader cfr;
	
	
	public GrcLogout(WebDriver driver){
		this.driver=driver;
	}
	
	public void logout() throws InterruptedException{
		    driver.findElement(By.id("btn_container")).click();
		    driver.findElement(By.xpath("//*[@id='jq-dropdown-1']/ul/li[4]/a")).click();
		    Thread.sleep(4000);
	}
	
	
	
	
}
