package fall_incident;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class CreateFall {

	WebDriver driver;
	ConfigFileReader cfr;
	
	public CreateFall(WebDriver driver){
		this.driver=driver;
	}
	
	public void create_fall_form() throws InterruptedException  {
		driver.findElement(By.id("DateofIncident")).click();
		driver.findElement(By.cssSelector(".today")).click();
		driver.findElement(By.cssSelector(".input-group-addon > .fa-clock-o")).click();
		driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).click();
		driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).sendKeys("8");
//		driver.findElement(By.cssSelector(".fa-folder-o")).click();
//		WebDriverWait  block = new WebDriverWait(driver,10);
//		WebElement indept = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='1171-label']")));
//		indept.click();
//		driver.findElement(By.xpath("//span[contains(.,\'OK\')]")).click();
		driver.findElement(By.cssSelector("#Severity_chosen b")).click();
		driver.findElement(By.cssSelector(".active-result:nth-child(2)")).click();
		driver.findElement(By.cssSelector("#ProgramId_chosen span")).click();
		// driver.findElement(By.cssSelector("#ProgramId_chosen.active-result:nth-child(2)")).click();
		driver.findElement(By.cssSelector("#LocationId_chosen span")).click();
		driver.findElement(By.cssSelector("#LocationId_chosen .active-result:nth-child(4)")).click();
		driver.findElement(By.id("PlaceofIncident")).click();
		driver.findElement(By.id("PlaceofIncident")).sendKeys("place");
		driver.findElement(By.cssSelector("#IncidentStatus_chosen span")).click();
		driver.findElement(By.cssSelector("#IncidentStatus_chosen .active-result:nth-child(1)")).click();
		driver.findElement(By.cssSelector(".chosen-default > span")).click();
		driver.findElement(By.cssSelector("#Priority_chosen .active-result:nth-child(1)")).click();
		driver.findElement(By.id("FallRiskAssessmentScore")).click();
		driver.findElement(By.id("FallRiskAssessmentScore")).sendKeys("fallrisk");
		driver.findElement(By.cssSelector("#ContributingFactors_chosen span")).click();
		// driver.findElement(By.cssSelector("#ContributingFactors_chosen .active-result:nth-child(2)")).click();
		driver.findElement(By.id("DateofFall")).click();
		driver.findElement(By.cssSelector(".today")).click();
		driver.findElement(By.id("DirectCauseOfFall")).click();
		driver.findElement(By.id("DirectCauseOfFall")).sendKeys("directcause");
		driver.findElement(By.id("Name")).click();
		driver.findElement(By.id("Name")).sendKeys("name");
		driver.findElement(By.id("NameofRN")).click();
		driver.findElement(By.id("NameofRN")).sendKeys("nameofrn");
		driver.findElement(By.id("OtherCommentsObservations")).click();
		driver.findElement(By.id("OtherCommentsObservations")).sendKeys("other");
		driver.findElement(By.id("ReportRN")).click();
		driver.findElement(By.id("ReportRN")).sendKeys("reportrn");
		driver.findElement(By.id("PostFallHuddle")).click();
		driver.findElement(By.id("PostFallHuddle")).sendKeys("post");
		driver.findElement(By.id("UnitCensus")).click();
		driver.findElement(By.id("UnitCensus")).sendKeys("unit");
		driver.findElement(By.id("LPN")).click();
		driver.findElement(By.id("LPN")).sendKeys("lpn");
		driver.findElement(By.id("NumberofStaff")).click();
		driver.findElement(By.id("NumberofStaff")).sendKeys("staff");
		driver.findElement(By.id("AcuityLevel")).click();
		driver.findElement(By.id("AcuityLevel")).sendKeys("level");
		driver.findElement(By.id("RN")).click();
		driver.findElement(By.id("RN")).sendKeys("rn");
		driver.findElement(By.id("MHW")).click();
		driver.findElement(By.id("MHW")).sendKeys("mhw");
		driver.findElement(By.id("OtherComments")).click();
		driver.findElement(By.id("OtherComments")).sendKeys("comment");
		driver.findElement(By.cssSelector(".btn-info")).click();
		Thread.sleep(4000);

	}

}
