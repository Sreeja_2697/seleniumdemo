package fall_incident;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class RootFall {

	

	WebDriver driver;
	ConfigFileReader cfr;
	
	public RootFall(WebDriver driver){
		this.driver=driver;
	}
	
	public void root_fall() throws InterruptedException{
		driver.findElement(By.cssSelector(".odd:nth-child(1) .btn > .ace-icon")).click();
		driver.findElement(By.linkText("Root Cause Analysis")).click();
		driver.findElement(By.id("ProblemStatement")).click();
		driver.findElement(By.id("ProblemStatement")).sendKeys("During DNV survey at the Thompson Peak Hospital multiple smoke/fire wall penetrations found not fully sealed.");
		driver.findElement(By.id("GoalStatement")).click();
		driver.findElement(By.id("GoalStatement")).sendKeys("Goals: Ensure that all �Life Safety� system are functioning properlyTargeted Process: All staff are trained on the system code requirements Scope: HonorHealth Scottsdale Thompson Peak Medical Center.");
		driver.findElement(By.cssSelector(".btn-info")).click();
		driver.findElement(By.cssSelector(".col-xs-12 > .btn")).click();
		WebDriverWait  block = new WebDriverWait(driver,10);
		WebElement ebutton = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='editRCA']")));
		WebElement einput;
		einput = ebutton.findElement(By.id("Description"));
		einput.sendKeys("describe");
		ebutton.findElement(By.cssSelector(".col-md-5:nth-child(2) .lbl")).click();
		driver.findElement(By.cssSelector(".pull-right:nth-child(1) > .btn")).click();
		WebElement button = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='collapseTwo']/div/div/button")));
		button.click();
		WebElement goal = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("goalMeasures")));
		WebElement minput;
		minput=goal.findElement(By.id("MeasureName"));
		minput.sendKeys("Sheal");
		driver.findElement(By.xpath("//*[@id='goalMeasures']/div/div/div[2]/div/button")).click();
		driver.findElement(By.cssSelector(".pull-right > .btn:nth-child(1)")).click();
		driver.findElement(By.id("FirstWhy")).click();
		driver.findElement(By.id("FirstWhy")).sendKeys("Immediate post-op notes not completed. Why?");
		driver.findElement(By.id("SecondWhy")).click();
		driver.findElement(By.id("SecondWhy")).sendKeys("Various reasons for several departments: Endoscopy did  not have a correct form and the IR form was missing language that indicated a physician reviewed the note. Why?");
		driver.findElement(By.id("ThirdWhy")).click();
		driver.findElement(By.id("ThirdWhy")).sendKeys("Lack of consistency and ownership of the process at a network level. Why?");
		driver.findElement(By.id("FourthWhy")).click();
		driver.findElement(By.id("FourthWhy")).sendKeys("It is seen as something inherent to surgical practice in that it shouldn�t need to be enforced/owned by the organization. Why?");
		driver.findElement(By.id("FifthWhy")).click();
		driver.findElement(By.id("FifthWhy")).sendKeys("Lack of process ownership and lack of a network champion to transform culture surrounding clinical documentation.");
		driver.findElement(By.cssSelector(".btn-info")).click();
		driver.findElement(By.cssSelector(".col-sm-offset-1")).click();
		WebElement pdsa = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='addPDSAForm']")));
		WebElement pinput;
		pinput= pdsa.findElement(By.id("RCAPlan"));
		pinput.sendKeys("Plan :Group convened and root cause analysis of problem statement performed. Memo sent to Medical Staff informing of non-compliance regarding immediate post-op completion and explanation of what is required per CMS guidelines. Audit form developed and implemented. The group expanded following the 2015 survey to include the hospital CMOs, surgical chairs, and anesthesia chairs.");
		pinput= pdsa.findElement(By.id("RCADo"));
		pinput.sendKeys("Do :5 charts audited per week in PACU to ensure immediate post-op notes are completed and to be used as education and real-time coaching for physicians.");
		pinput= pdsa.findElement(By.id("RCAStudy"));
		pinput.sendKeys("Study :Audits are addressed in real time, on day that audit is performed. Data from HBI will be pulled to audit for physician compliance to completing the immediate post-op notes.");
		pinput= pdsa.findElement(By.id("RCAAction"));
		pinput.sendKeys("Action :Audits that show non-compliance are discussed with surgeon when surgeon is still available in real-time. Results from HBI regarding % charts complete will be taken to campus CMO. Campus CMO will share this information with the physician and Medical Staff will be asked to send a letter to the physician stating their non-compliance with the standard.");
		driver.findElement(By.cssSelector(".pull-right > .btn")).click();
		driver.findElement(By.cssSelector(".btn:nth-child(3)")).click();
		driver.findElement(By.id("TargetAction")).click();
		driver.findElement(By.id("TargetAction")).sendKeys("targetaction");
		driver.findElement(By.cssSelector(".input-group-addon > .fa")).click();
		driver.findElement(By.cssSelector(".today")).click();
		driver.findElement(By.cssSelector("#Responsibility_chosen span")).click();
		driver.findElement(By.cssSelector(".pull-right > .btn")).click();
		driver.findElement(By.xpath("//a[contains(@href, '#collapseFour')]")).click();
		WebElement task = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("CreateTaskButton")));
		task.click();
		WebElement ctask = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("reassignDiv")));
		WebElement cinput;
		cinput= ctask.findElement(By.id("Title"));
		cinput.sendKeys("Talk to Robert to order sealer");
		cinput= ctask.findElement(By.id("Description"));
		cinput.sendKeys("Talk to Robert to order sealer");
		ctask.findElement(By.cssSelector(".fa-user")).click();
		WebElement select = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='selectuser-table']/tbody/tr[3]/td[1]/center/input[2]")));
		select.click();
		driver.findElement(By.xpath("//*[@id='reassignDiv']/form/div/div/div[2]/button")).click();
		ctask.findElement(By.cssSelector(".fa-calendar")).click();
		WebElement tdate = block.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".today")));
		tdate.click();
		driver.findElement(By.cssSelector(".modal-footer:nth-child(4) > #closeNotesButton")).click();
		driver.findElement(By.cssSelector(".accordion-toggle")).click();
		WebElement abutton = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@id='tasklisttable']/tbody/tr/td[5]/div/span/a/i")));
		Thread.sleep(4000);
	}
	
	
}
