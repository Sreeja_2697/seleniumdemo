package fall_incident;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AttachFall {

	WebDriver driver;
	
	public AttachFall(WebDriver driver){
		this.driver=driver;
	}
	
	 public void fall_attachment_save() throws InterruptedException{
 	    driver.findElement(By.linkText("Attachments")).click();
 	    WebDriverWait  block = new WebDriverWait(driver,10);
 	    WebElement button = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='collapseTwo']/div/div/button")));
 	    button.click();
 	    WebElement attach = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("createAttachmentForm")));
 	    attach.findElement(By.id("Title")).click();
 	    attach.findElement(By.id("Title")).sendKeys("title");
 	    WebElement upload = attach.findElement(By.xpath("//input[@id='file']"));
 	    upload.sendKeys("C:\\BrowserDrivers\\Screenshot (1).png");
 	    driver.findElement(By.id("Upload")).click();
 	    Thread.sleep(3000);
 }
	
}
