package fall_incident;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NoteFall {


	WebDriver driver;
	
	public NoteFall(WebDriver driver){
		this.driver=driver;
	}
	 public void fall_note_save() throws InterruptedException{
	    	driver.findElement(By.linkText("Notes")).click();
	    	 WebDriverWait  block = new WebDriverWait(driver,10);
	    	WebElement notes = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("noteForm")));
	    	notes.findElement(By.name("Title")).click();
	    	notes.findElement(By.name("Title")).sendKeys("title");
	    	notes.findElement(By.name("Note")).click();
	    	notes.findElement(By.name("Note")).sendKeys("note");
	        driver.findElement(By.cssSelector(".btn-lg")).click();
	        driver.findElement(By.linkText("Notes")).click();
	        Thread.sleep(3000);
	    	
	    }
	
	
}
