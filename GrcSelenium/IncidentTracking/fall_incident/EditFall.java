package fall_incident;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class EditFall {


	WebDriver driver;
	ConfigFileReader cfr;
	
	public EditFall(WebDriver driver){
		this.driver=driver;
	}
	
	public void edit_fall_form() throws InterruptedException
	{
		driver.findElement(By.cssSelector(".odd:nth-child(1) .btn > .ace-icon")).click();
		driver.findElement(By.linkText("Details")).click();
		driver.findElement(By.cssSelector(".input-group-addon > .fa-clock-o")).click();
		driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).click();
		driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).clear();
		driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).sendKeys("4");
//		driver.findElement(By.cssSelector(".fa-folder-o")).click();
//		WebDriverWait  block = new WebDriverWait(driver,10);
//		WebElement indept = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='1171-label']")));
//		indept.click();
//		driver.findElement(By.xpath("//span[contains(.,\'OK\')]")).click();
		driver.findElement(By.id("PlaceofIncident")).click();
		driver.findElement(By.id("PlaceofIncident")).clear();
		driver.findElement(By.id("PlaceofIncident")).sendKeys("place");
		driver.findElement(By.id("FallRiskAssessmentScore")).click();
		driver.findElement(By.id("FallRiskAssessmentScore")).clear();
		driver.findElement(By.id("FallRiskAssessmentScore")).sendKeys("fallrisk");
		driver.findElement(By.id("DirectCauseOfFall")).click();
		driver.findElement(By.id("DirectCauseOfFall")).clear();
		driver.findElement(By.id("DirectCauseOfFall")).sendKeys("directcause");
		driver.findElement(By.id("Name")).click();
		driver.findElement(By.id("Name")).clear();
		driver.findElement(By.id("Name")).sendKeys("name");
		driver.findElement(By.id("NameofRN")).click();
		driver.findElement(By.id("NameofRN")).clear();
		driver.findElement(By.id("NameofRN")).sendKeys("nameofrn");
		driver.findElement(By.id("OtherCommentsObservations")).click();
		driver.findElement(By.id("OtherCommentsObservations")).clear();
		driver.findElement(By.id("OtherCommentsObservations")).sendKeys("other");
		driver.findElement(By.id("ReportRN")).click();
		driver.findElement(By.id("ReportRN")).clear();
		driver.findElement(By.id("ReportRN")).sendKeys("reportrn");
		driver.findElement(By.id("PostFallHuddle")).click();
		driver.findElement(By.id("PostFallHuddle")).clear();
		driver.findElement(By.id("PostFallHuddle")).sendKeys("post");
		driver.findElement(By.id("UnitCensus")).click();
		driver.findElement(By.id("UnitCensus")).clear();
		driver.findElement(By.id("UnitCensus")).sendKeys("unit");
		driver.findElement(By.id("LPN")).click();
		driver.findElement(By.id("LPN")).clear();
		driver.findElement(By.id("LPN")).sendKeys("lpn");
		driver.findElement(By.id("NumberofStaff")).click();
		driver.findElement(By.id("NumberofStaff")).clear();
		driver.findElement(By.id("NumberofStaff")).sendKeys("staff");
		driver.findElement(By.id("AcuityLevel")).click();	
		driver.findElement(By.id("AcuityLevel")).clear();
		driver.findElement(By.id("AcuityLevel")).sendKeys("level");
		driver.findElement(By.id("RN")).click();
		driver.findElement(By.id("RN")).clear();
		driver.findElement(By.id("RN")).sendKeys("rn");
		driver.findElement(By.id("MHW")).click();
		driver.findElement(By.id("MHW")).clear();
		driver.findElement(By.id("MHW")).sendKeys("mhw");
		driver.findElement(By.id("OtherComments")).click();
		driver.findElement(By.id("OtherComments")).clear();
		driver.findElement(By.id("OtherComments")).sendKeys("other");
		driver.findElement(By.cssSelector(".pull-right > .btn-info")).click();
		System.out.println("Fall Form has been updated");
		Thread.sleep(3000);
	}

}
