package events;

import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class Event {
	
	
	WebDriver driver;
	ConfigFileReader cfr;
	 private String today;
	
	public Event(WebDriver driver){
		this.driver=driver;
	}
	  public void event_form_save() throws InterruptedException  {
		 	driver.findElement(By.cssSelector("button[onclick^='javascript: addEvent']")).click();
		 	WebDriverWait  block = new WebDriverWait(driver,10);
		    WebElement modal = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("addEventForm")));
		    WebElement input;
		    input=modal.findElement(By.id("Title"));
		    input.sendKeys("abc");
		    modal.findElement(By.cssSelector(".chosen-choices")).click();
		    modal.findElement(By.cssSelector(".chosen-results > li:nth-child(1)")).click();
		    modal.findElement(By.xpath("//form[@id='addEventForm']/div/div/div/div[4]/div/div/div/div/span/i")).click();
		    WebElement dateWidgetFrom = modal.findElement(By.xpath("/html/body/div[6]/div[1]/table/tbody"));
		    today = getCurrentDay();
		    System.out.println("Today's number: " + today + "\n");
		    //List<WebElement> rows = dateWidgetFrom.findElements(By.tagName("tr"));

		    //This are the columns of the from date picker table
		    List<WebElement> columns = dateWidgetFrom.findElements(By.tagName("td"));

		    //DatePicker is a table. Thus we can navigate to each cell
		    //and if a cell matches with the current date then we will click it.
		    for (WebElement cell: columns) {
		        /*
		        //If you want to click 18th Date
		        if (cell.getText().equals("18")) {
		        */
		        //Select Today's Date
		        if (cell.getText().equals(today)) {
		            cell.click();
		            break;
		        }
		    }

		    //Wait for 4 Seconds to see Today's date selected.
		    try {
		        Thread.sleep(4000);
		    } catch (InterruptedException e) {
		        e.printStackTrace();
		    }
		    input=modal.findElement(By.id("Description"));
		    input.sendKeys("cde");
		     input=modal.findElement(By.id("addEventButton"));
		    input.click();
		    Thread.sleep(4000);
		    driver.findElement(By.cssSelector(".odd:nth-child(1) .red > .ace-icon")).click();
		    WebElement alert = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[4]/div[3]/div/button/span")));
			    alert.click();
			 Thread.sleep(3000);
			driver.findElement(By.cssSelector("button[onclick^='javascript: addEvent']")).click();
			WebDriverWait  block1 = new WebDriverWait(driver,10);
		    WebElement modal1 = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("addEventForm")));
		    WebElement input1;
		    input1=modal1.findElement(By.id("addEventButton"));
		    input1.click();
		    
		Thread.sleep(4000);
		   
		    }
	//Get The Current Day
	    private String getCurrentDay (){
	        //Create a Calendar Object
	        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

	        //Get Current Day as a number
	        int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
	        System.out.println("Today Int: " + todayInt +"\n");

	        //Integer to String Conversion
	        String todayStr = Integer.toString(todayInt);
	        System.out.println("Today Str: " + todayStr + "\n");

	        return todayStr;
	    }
	
}
