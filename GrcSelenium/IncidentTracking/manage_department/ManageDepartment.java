package manage_department;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class ManageDepartment {
	WebDriver driver;
	ConfigFileReader cfr;
	
	public ManageDepartment(WebDriver driver){
		this.driver=driver;
	}
	public void manage_department() throws InterruptedException  {
		  driver.findElement(By.cssSelector(".btn-sm:nth-child(1)")).click();
	 		 WebDriverWait  block = new WebDriverWait(driver,10);
	 	   WebElement adept = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("addDepartmentDiv")));
	 	   WebElement add1;
	 	   add1= adept.findElement(By.id("Name"));
	 	   add1.sendKeys("First");
	 	   driver.findElement(By.id("saveDeptBtn")).click();
	 	   Thread.sleep(3000);
	 	  driver.findElement(By.xpath("//li[10]/div/span/span")).click();
	 	    driver.findElement(By.cssSelector(".btn-danger")).click();
	 	   WebElement alert = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[6]/div[3]/div/button[1]/span")));
	 	  alert.click();
	 	  Thread.sleep(3000);
	 	 WebElement rid = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='1177-label']")));
		    rid.click();
		 driver.findElement(By.cssSelector(".btn:nth-child(2)")).click();
		 WebElement rdept = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("editDepartmentDiv")));
	WebElement rename;
	rename= rdept.findElement(By.id("Name"));
	rename.clear();
	rename.sendKeys("Clinic");
	driver.findElement(By.cssSelector(".fa-share")).click();
	Thread.sleep(4000);
	WebElement rid2 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='171-label']")));
	rid2.click();
	Thread.sleep(3000);
	WebElement deuser = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='main-container']/div[2]/div/div/div[1]/div/div/div[2]/div[1]/div/div/button[4]")));
	deuser.click();
	Thread.sleep(2000);
	WebElement a1 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='manageDepartmentModal']/div/div/button")));
	a1.click();
	WebElement ai1 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='selectuserstablelist']/tbody/tr[3]/td[2]/label/span")));
	ai1.click();
	driver.findElement(By.cssSelector(".btn-default")).click();
	//assign admin
	WebElement ai2 = block.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".odd:nth-child(1) .lbl")));
	ai2.click();
	driver.findElement(By.cssSelector(".btn-primary:nth-child(4)")).click();
	Thread.sleep(3000);
	//delete admin
	WebElement ai3 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@id='deptuserstablelist']/tbody/tr/td[2]/label/span")));
	ai3.click();
	Thread.sleep(3000);
	driver.findElement(By.xpath("//*[@id='manageDepartmentModal']/div/div/button[4]")).click();
	Thread.sleep(3000);

	//Delete User
	WebElement ai4 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='deptuserstablelist']/tbody/tr/td[2]/label/span")));
	ai4.click();
	WebElement ai5 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='manageDepartmentModal']/div/div/button[2]")));
	ai5.click();
	WebElement ai6 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[10]/div[3]/div/button[1]/span")));
	ai6.click();
	}

}
