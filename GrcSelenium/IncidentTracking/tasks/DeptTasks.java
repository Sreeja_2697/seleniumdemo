package tasks;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class DeptTasks {


	WebDriver driver;
	ConfigFileReader cfr;
	
	
	public DeptTasks(WebDriver driver){
		this.driver=driver;
	}
	public void dept_tasks() throws InterruptedException{
		 driver.findElement(By.cssSelector(".odd:nth-child(3) .ace-icon")).click();
		 WebDriverWait  block = new WebDriverWait(driver,10);
	   WebElement task1 = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("closeActionWindowModal")));
		 task1.findElement(By.id("EditNotes")).sendKeys("goal");
		 driver.findElement(By.id("closeNotesButton")).click();
		 driver.findElement(By.cssSelector(".odd:nth-child(3) .grey:nth-child(2) .bigger-120")).click();
		 WebElement task2 = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("reassignDiv")));
		 WebElement se1 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='reassign-table']/tbody/tr[3]/td[1]/center/input[4]")));
		 se1.click();
		 driver.findElement(By.cssSelector(".col-xs-12 > .modal-footer > .btn")).click();
		 Thread.sleep(3000);
	}
}
