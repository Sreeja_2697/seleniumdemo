package general_incident;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class EditIncident {
	

	WebDriver driver;
	ConfigFileReader cfr;
	
	
	public EditIncident(WebDriver driver){
		this.driver=driver;
	}
	
	
	public void edit_general() throws InterruptedException{
		 driver.findElement(By.cssSelector(".odd:nth-child(1) .btn > .ace-icon")).click();
		 driver.findElement(By.linkText("Details")).click();
		 driver.findElement(By.cssSelector(".row:nth-child(1) > .form-group .col-md-2:nth-child(3) .input-group-addon")).click();
		    driver.findElement(By.cssSelector(".today")).click();
		    driver.findElement(By.cssSelector(".col-md-2:nth-child(3) .fa-calendar")).click();
		    driver.findElement(By.cssSelector(".today")).click();
		    driver.findElement(By.id("TimeOfIncident")).click();
		    driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).click();
		    driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).clear();
		    driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).sendKeys("2");
		    driver.findElement(By.cssSelector(".input-group-addon > .fa-list")).click();
		      WebDriverWait  block = new WebDriverWait(driver,10);
			    WebElement indept = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("rmsTreeContainer")));
			    WebElement indept1 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='Assault-label']")));
		        indept1.click();
			    WebElement indept2 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@id=\'7\']/span/span[2]")));
		indept2.click();
		indept.findElement(By.xpath("/html/body/div[4]/div[3]/div/button/span")).click();
//		      driver.findElement(By.cssSelector(".fa-folder-o")).click();
//			    WebElement indept4 = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='1171-label']")));
//			    indept4.click();
//			    driver.findElement(By.xpath("/html/body/div[5]/div[3]/div/button/span")).click();
			    driver.findElement(By.id("PlaceOfIncident")).click();
			    driver.findElement(By.id("PlaceOfIncident")).clear();
			    driver.findElement(By.id("PlaceOfIncident")).sendKeys("Corner of South entrance Corrridor");
//			    driver.findElement(By.cssSelector("#IncidentStatus_chosen span")).click();
//			      driver.findElement(By.cssSelector("#IncidentStatus_chosen .active-result:nth-child(1)")).click();
//			      driver.findElement(By.cssSelector(".chosen-default > span")).click();
//			      driver.findElement(By.cssSelector("#Priority_chosen .active-result:nth-child(1)")).click();
//			      driver.findElement(By.cssSelector("#Injury_chosen span")).click();
//			    // driver.findElement(By.xpath("//*[@id='Injury_chosen']/div/ul/li[3]")).click();
//			      driver.findElement(By.cssSelector("#Severity_chosen b")).click();
//			      driver.findElement(By.cssSelector("#Severity_chosen .active-result:nth-child(2)")).click();
//			      driver.findElement(By.cssSelector(".chosen-choices")).click();
//			      driver.findElement(By.cssSelector(".chosen-results:nth-child(1) > li:nth-child(1)")).click();
		    driver.findElement(By.id("Narrative")).click();
		    driver.findElement(By.id("Narrative")).clear();
		      driver.findElement(By.id("Narrative")).sendKeys("The above patient fell off while walking in the corridor as the floor surface was slippery");
//		      driver.findElement(By.cssSelector("#NurseMedReportFor_chosen span")).click();
//		      driver.findElement(By.xpath("//*[@id='NurseMedReportFor_chosen']/div/ul/li[2]")).click();
//		      driver.findElement(By.id("NurseDateOfIncident")).click();
//		      driver.findElement(By.cssSelector(".today")).clear();
//		      driver.findElement(By.cssSelector(".today")).click();
		      driver.findElement(By.id("NurseTimeOfIncident")).click();
		      driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).click();
		      driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).clear();
		      driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).sendKeys("2");
		      driver.findElement(By.cssSelector(".bootstrap-timepicker-minute")).click();
		      driver.findElement(By.id("NurseFirstName")).click();
		      driver.findElement(By.id("NurseFirstName")).clear();
		      driver.findElement(By.id("NurseFirstName")).sendKeys("Thomson");
		      driver.findElement(By.id("NurseLastName")).click();
		      driver.findElement(By.id("NurseLastName")).clear();
		      driver.findElement(By.id("NurseLastName")).sendKeys("Neil");
		      driver.findElement(By.id("NurseTitle")).click();
		      driver.findElement(By.id("NurseTitle")).clear();
		      driver.findElement(By.id("NurseTitle")).sendKeys("Nurse");
		      driver.findElement(By.id("NurseReport")).click();
		      driver.findElement(By.id("NurseReport")).clear();
		      driver.findElement(By.id("NurseReport")).sendKeys("The above incident was noticed by one of the visitor and same was reported by the front desk");
//		      driver.findElement(By.cssSelector("#PhysicianMedReportFor_chosen span")).click();
			     // driver.findElement(By.xpath("//*[@id='PhysicianMedReportFor_chosen']/div/ul/li[2]")).click();
//			      driver.findElement(By.id("PhysicianDateOfIncident")).click();
//			      driver.findElement(By.cssSelector(".today")).clear();
//			      driver.findElement(By.cssSelector(".today")).click();
			      driver.findElement(By.id("PhysicianTimeOfIncident")).click();
			      driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).click();
			      driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).clear();
			      driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).sendKeys("5");
			      driver.findElement(By.cssSelector(".bootstrap-timepicker-minute")).click();
		      driver.findElement(By.id("PhysicianFirstName")).click();
		      driver.findElement(By.id("PhysicianFirstName")).clear();
		      driver.findElement(By.id("PhysicianFirstName")).sendKeys("James");
		      driver.findElement(By.id("PhysicianLastName")).click();
		      driver.findElement(By.id("PhysicianLastName")).clear();
		      driver.findElement(By.id("PhysicianLastName")).sendKeys("Moroe");
		      driver.findElement(By.id("PhysicianTitle")).click();
		      driver.findElement(By.id("PhysicianTitle")).clear();
		      driver.findElement(By.id("PhysicianTitle")).sendKeys("Physican");
		      driver.findElement(By.id("PhysicianReport")).click();
		      driver.findElement(By.id("PhysicianReport")).clear();
		      driver.findElement(By.id("PhysicianReport")).sendKeys("He was treated with first aid and later he was discharged home");
		      driver.findElement(By.cssSelector(".pull-right > .btn-info")).click();
		      System.out.println("General Form has been updated");
	     Thread.sleep(3000);
	}
	
	
	
	
	
	
	
	
	
	
	
}
