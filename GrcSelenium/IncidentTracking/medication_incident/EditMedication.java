package medication_incident;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class EditMedication {

	
	WebDriver driver;
	ConfigFileReader cfr;
	
	public EditMedication(WebDriver driver){
		this.driver=driver;
	}
	
	 public void edit_medication() throws InterruptedException{
	  driver.findElement(By.cssSelector(".odd:nth-child(1) .btn > .ace-icon")).click();
	  driver.findElement(By.linkText("Details")).click();
	  driver.findElement(By.cssSelector("#Severity_chosen span")).click();
	  driver.findElement(By.cssSelector(".result-selected")).click();
	  driver.findElement(By.cssSelector("#SeverityCategory_chosen span")).click();
	  driver.findElement(By.cssSelector("#SeverityCategory_chosen .result-selected")).click();
	  driver.findElement(By.cssSelector(".fa-folder-o")).click();
		 WebDriverWait  block = new WebDriverWait(driver,10);
	  WebElement indept = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='1171-label']")));
	  indept.click();
	  driver.findElement(By.xpath("//span[contains(.,\'OK\')]")).click();
	  driver.findElement(By.cssSelector("#LOC_chosen span")).click();
	  driver.findElement(By.cssSelector("#LOC_chosen .active-result:nth-child(2)")).click();
//	  driver.findElement(By.id("DateOfIncident")).click();
//	  driver.findElement(By.cssSelector(".today")).click();
//	  driver.findElement(By.cssSelector(".col-md-2:nth-child(3) .fa")).click();
//	  driver.findElement(By.cssSelector(".today")).click();
	  driver.findElement(By.id("TimeOfIncident")).click();
	  driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).click();
	  driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).clear();
	  driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).sendKeys("5");
//	  driver.findElement(By.cssSelector(".col-md-4:nth-child(3)")).click();
//	  driver.findElement(By.cssSelector("#ErrorOrigin_chosen span")).click();
//	  driver.findElement(By.cssSelector("#ErrorOrigin_chosen .active-result:nth-child(3)")).click();
	  driver.findElement(By.id("NumberOfOccurrences")).click();
	  driver.findElement(By.id("NumberOfOccurrences")).clear();
	  driver.findElement(By.id("NumberOfOccurrences")).sendKeys("5");
//	  driver.findElement(By.cssSelector(".col-md-2:nth-child(4) .fa-calendar")).click();
//	  driver.findElement(By.cssSelector(".today:nth-child(5)")).click();
	  //driver.findElement(By.cssSelector(".chosen-choices")).click();
	 // driver.findElement(By.cssSelector(".chosen-results:nth-child(1) > li")).click();
//	  driver.findElement(By.cssSelector("#IncidentOccuredStage_chosen span")).click();
//	  driver.findElement(By.cssSelector("#IncidentOccuredStage_chosen .active-result:nth-child(3)")).click();
	  driver.findElement(By.id("BrieflyDescribe")).click();
	  driver.findElement(By.id("BrieflyDescribe")).clear();
	  driver.findElement(By.id("BrieflyDescribe")).sendKeys("briefly");
	  //driver.findElement(By.cssSelector("#PrimaryProblemIdentified_chosen span")).click();
	  driver.findElement(By.cssSelector(".row > .col-md-12")).click();
	  driver.findElement(By.id("CriticalPatientInformationMissing")).click();
	  driver.findElement(By.id("CriticalPatientInformationMissing")).click();
	  driver.findElement(By.id("CriticalPatientInformationMissingExplanation")).click();
	  driver.findElement(By.id("CriticalPatientInformationMissingExplanation")).clear();
	  driver.findElement(By.id("CriticalPatientInformationMissingExplanation")).sendKeys("cpatient");
	  driver.findElement(By.id("CriticalDrugInformationMissing")).click();
	  driver.findElement(By.id("CriticalDrugInformationMissing")).click();
	  driver.findElement(By.id("CriticalDrugInformationMissingExplanation")).click();
	  driver.findElement(By.id("CriticalDrugInformationMissingExplanation")).clear();
	  driver.findElement(By.id("CriticalDrugInformationMissingExplanation")).sendKeys("cdrug");
	  driver.findElement(By.id("MiscommunicationOfDrugOrder")).click();
	  driver.findElement(By.id("MiscommunicationOfDrugOrder")).click();
	  driver.findElement(By.id("MiscommunicationOfDrugOrderExplanation")).click();
	  driver.findElement(By.id("MiscommunicationOfDrugOrderExplanation")).clear();
	  driver.findElement(By.id("MiscommunicationOfDrugOrderExplanation")).sendKeys("drugorder");
	  driver.findElement(By.id("DrugNameLabelOrPackagingProblem")).click();
	  driver.findElement(By.id("DrugNameLabelOrPackagingProblem")).click();
	  driver.findElement(By.id("DrugNameLabelOrPackagingProblemExplanation")).click();
	  driver.findElement(By.id("DrugNameLabelOrPackagingProblemExplanation")).clear();
	  driver.findElement(By.id("DrugNameLabelOrPackagingProblemExplanation")).sendKeys("drugpack");
	  driver.findElement(By.id("DrugStorageOrDeliveryProblem")).click();
	  driver.findElement(By.id("DrugStorageOrDeliveryProblem")).click();
	  driver.findElement(By.id("DrugStorageOrDeliveryProblemExplanation")).click();
	  driver.findElement(By.id("DrugStorageOrDeliveryProblemExplanation")).clear();
	  driver.findElement(By.id("DrugStorageOrDeliveryProblemExplanation")).sendKeys("drugstorage");
	  driver.findElement(By.id("EnvironmentalStaffingOrWorkflowProblems")).click();
	  driver.findElement(By.id("EnvironmentalStaffingOrWorkflowProblems")).click();
	  driver.findElement(By.id("EnvironmentalStaffingOrWorkflowProblemsExplanation")).click();
	  driver.findElement(By.id("EnvironmentalStaffingOrWorkflowProblemsExplanation")).clear();
	  driver.findElement(By.id("EnvironmentalStaffingOrWorkflowProblemsExplanation")).sendKeys("workflow");
	  driver.findElement(By.id("StaffEducation")).click();
	  driver.findElement(By.id("StaffEducation")).click();
	  driver.findElement(By.id("StaffEducationExplanation")).click();
	  driver.findElement(By.id("StaffEducationExplanation")).clear();
	  driver.findElement(By.id("StaffEducationExplanation")).sendKeys("staff");
	  driver.findElement(By.id("PatientEducationProblem")).click();
	  driver.findElement(By.id("PatientEducationProblem")).click();
	  driver.findElement(By.id("PatientEducationProblemExplanation")).click();
	  driver.findElement(By.id("PatientEducationProblemExplanation")).clear();
	  driver.findElement(By.id("PatientEducationProblemExplanation")).sendKeys("patient");
	  driver.findElement(By.id("QualityControlOrIndependentCheckSystems")).click();
	  driver.findElement(By.id("QualityControlOrIndependentCheckSystems")).click();
	  driver.findElement(By.id("QualityControlOrIndependentCheckSystemsExplanation")).click();
	  driver.findElement(By.id("QualityControlOrIndependentCheckSystemsExplanation")).clear();
	  driver.findElement(By.id("QualityControlOrIndependentCheckSystemsExplanation")).sendKeys("quality");
	  driver.findElement(By.id("Other")).click();
	  driver.findElement(By.id("Other")).click();
	  driver.findElement(By.id("OtherExplanation")).click();
	  driver.findElement(By.id("OtherExplanation")).clear();
	  driver.findElement(By.id("OtherExplanation")).sendKeys("other");
	  driver.findElement(By.cssSelector("#COE_chosen span")).click();
	  driver.findElement(By.id("Comments")).click();
	  driver.findElement(By.id("ReportedTo")).click();
	  driver.findElement(By.id("ReportedTo")).clear();
	  driver.findElement(By.id("ReportedTo")).sendKeys("reportto");
	  driver.findElement(By.id("EMBy")).click();
	  driver.findElement(By.id("EMBy")).clear();
	  driver.findElement(By.id("EMBy")).sendKeys("emby");
	  driver.findElement(By.id("EDBy")).click();
	  driver.findElement(By.id("EDBy")).clear();
	  driver.findElement(By.id("EDBy")).sendKeys("edby");
	  driver.findElement(By.id("OrderedBy")).click();
	  driver.findElement(By.id("OrderedBy")).clear();
	  driver.findElement(By.id("OrderedBy")).sendKeys("orderby");
	  driver.findElement(By.id("Comments")).click();
	  driver.findElement(By.id("Comments")).clear();
	  driver.findElement(By.id("Comments")).sendKeys("comment");
	  driver.findElement(By.cssSelector(".col-md-6 > #Comments")).click();
	  driver.findElement(By.cssSelector(".col-md-6 > #Comments")).clear();
	  driver.findElement(By.cssSelector(".col-md-6 > #Comments")).sendKeys("comments");
	  driver.findElement(By.id("FollowUp")).click();
	  driver.findElement(By.id("FollowUp")).clear();
	  driver.findElement(By.id("FollowUp")).sendKeys("followup");
	  driver.findElement(By.id("CompletedBy")).click();
	  driver.findElement(By.id("CompletedBy")).clear();
	  driver.findElement(By.id("CompletedBy")).sendKeys("d");
//	  driver.findElement(By.id("CompletedOn")).click();
//	  driver.findElement(By.cssSelector(".today:nth-child(5)")).click();
//	  driver.findElement(By.cssSelector(".col-md-3 .fa")).click();
//	  driver.findElement(By.cssSelector(".today:nth-child(5)")).click();
	  driver.findElement(By.cssSelector("#IncidentStatus_chosen span")).click();
	  // driver.findElement(By.cssSelector("#IncidentStatus_chosen .active-result:nth-child(1)")).click();
	 //  driver.findElement(By.cssSelector(".chosen-default > span")).click();
	  //  driver.findElement(By.cssSelector("#Priority_chosen .active-result:nth-child(1)")).click();
	  driver.findElement(By.cssSelector(".pull-right > .btn-info")).click();
	  	Thread.sleep(3000);
	    }
}
