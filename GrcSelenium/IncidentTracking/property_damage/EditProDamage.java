package property_damage;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import dataprovider.ConfigFileReader;

public class EditProDamage {


	WebDriver driver;
	ConfigFileReader cfr;
	
	public EditProDamage(WebDriver driver){
		this.driver=driver;
	}
	
	public void edit_property_damage() throws InterruptedException{
		driver.findElement(By.cssSelector(".odd:nth-child(1) .btn > .ace-icon")).click();
		driver.findElement(By.linkText("Details"));
		driver.findElement(By.cssSelector(".pull-right > .btn-info")).click();   
		Thread.sleep(2000);   	
	}
}
