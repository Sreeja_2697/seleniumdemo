package graphical_charts;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import dataprovider.ConfigFileReader;

public class IncidentBySeverity {

	
	WebDriver driver;
	ConfigFileReader cfr;
	
	
	public IncidentBySeverity(WebDriver driver){
		this.driver=driver;
	}


	public void incident_by_severity_pdf() throws InterruptedException{
		
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 driver.findElement(By.linkText("Incident by Severity")).click();
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("reportParam")));
		 iny.findElement(By.id("from")).click();
		 WebElement hdate = block.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("tr:nth-child(3) > .day:nth-child(3)")));
		 hdate.click();
		 iny.findElement(By.id("to")).click();
		 WebElement hdate1 = block.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("tr:nth-child(3) > .day:nth-child(4)")));
		 hdate1.click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
	Thread.sleep(3000);
		
	}

	public void incident_by_severity_excel() throws InterruptedException{
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("formatExcel")));
		 iny.click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
		
	Thread.sleep(3000);

		
	}

	public void incident_by_severity_word() throws InterruptedException{
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("formatWord")));
		 iny.click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
	Thread.sleep(3000);

	}

	public void incident_by_severity_image() throws InterruptedException{
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("formatImage")));
		 iny.click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
	Thread.sleep(3000);
		
	}

	public void incident_by_severity_view() throws InterruptedException{
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("formatView")));
		 iny.click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
		 Thread.sleep(3000);
		 driver.findElement(By.cssSelector(".btn-danger")).click();
	Thread.sleep(3000);

		
	}
}
