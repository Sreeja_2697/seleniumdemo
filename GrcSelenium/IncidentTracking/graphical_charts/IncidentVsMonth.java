package graphical_charts;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import dataprovider.ConfigFileReader;

public class IncidentVsMonth {


	
	WebDriver driver;
	ConfigFileReader cfr;
	
	
	public IncidentVsMonth(WebDriver driver){
		this.driver=driver;
	}


	public void incident_vs_month_pdf() throws InterruptedException{
	     driver.findElement(By.linkText("No of Incident Vs Month")).click();
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("Year_chosen")));
		 iny.findElement(By.cssSelector(".chosen-single > span")).click();
		 iny.findElement(By.cssSelector(".active-result:nth-child(3)")).click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
		// driver.findElement(By.cssSelector(".btn-danger")).click();
	Thread.sleep(3000);
		
	}

	public void incident_vs_month_excel() throws InterruptedException{
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 WebElement inm = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("Year_chosen")));
		 inm.findElement(By.cssSelector(".chosen-single > span")).click();
		 inm.findElement(By.cssSelector(".active-result:nth-child(4)")).click();
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("formatExcel")));
		 iny.click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
		
	Thread.sleep(3000);

		
	}
	public void incident_vs_month_word() throws InterruptedException{
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 WebElement inm = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("Year_chosen")));
		 inm.findElement(By.cssSelector(".chosen-single > span")).click();
		 inm.findElement(By.cssSelector(".active-result:nth-child(4)")).click();
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("formatWord")));
		 iny.click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
	Thread.sleep(3000);

	}

	public void incident_vs_month_image() throws InterruptedException{
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 WebElement inm = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("Year_chosen")));
		 inm.findElement(By.cssSelector(".chosen-single > span")).click();
		 inm.findElement(By.cssSelector(".active-result:nth-child(4)")).click();
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("formatImage")));
		 iny.click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
	Thread.sleep(3000);
		
	}

	public void incident_vs_month_view() throws InterruptedException{
		 WebDriverWait  block = new WebDriverWait(driver,10);
		 WebElement inm = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("Year_chosen")));
		 inm.findElement(By.cssSelector(".chosen-single > span")).click();
		 inm.findElement(By.cssSelector(".active-result:nth-child(4)")).click();
		 WebElement iny = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("formatView")));
		 iny.click();
		 driver.findElement(By.cssSelector(".btn-success")).click();
		 Thread.sleep(3000);
		 driver.findElement(By.cssSelector(".btn-danger")).click();
	Thread.sleep(3000);

		
	}
}
