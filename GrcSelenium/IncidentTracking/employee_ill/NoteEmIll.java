package employee_ill;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NoteEmIll {
	
	
	WebDriver driver;
	
	public NoteEmIll(WebDriver driver){
		this.driver=driver;
	}
	 public void employee_note_save() throws InterruptedException{
    	 WebElement note=driver.findElement(By.linkText("Notes"));
    		  note.click();
    		  WebDriverWait  block = new WebDriverWait(driver,10);
    	    	WebElement notes = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("noteForm")));
    	    	notes.findElement(By.name("Title")).click();
    	    	notes.findElement(By.name("Title")).sendKeys("ABCD");
    	    	notes.findElement(By.name("Note")).click();
    	    	notes.findElement(By.name("Note")).sendKeys("GDG");
    	        driver.findElement(By.cssSelector(".btn-lg")).click();
    	  
        Thread.sleep(3000);
    	
    }
}
