package employee_ill;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class DeleteEmIll {
	
	WebDriver driver;
	 ConfigFileReader cfr;
	public DeleteEmIll(WebDriver driver){
		this.driver=driver;
	}

	 public void delete_employee_ill() throws InterruptedException{
	  	 driver.findElement(By.cssSelector(".odd:nth-child(1) .btn > .ace-icon")).click();
	    	  driver.findElement(By.linkText("Delete")).click();
	   	  WebDriverWait  block = new WebDriverWait(driver,10);
	   	    WebElement alert = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[4]/div[3]/div/button/span")));
	   	    alert.click();
	   	   Thread.sleep(3000);
	  	 
	    }
}
