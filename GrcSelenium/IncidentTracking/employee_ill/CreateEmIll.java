package employee_ill;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataprovider.ConfigFileReader;

public class CreateEmIll {

	
	WebDriver driver;
	 ConfigFileReader cfr;
	public CreateEmIll(WebDriver driver){
		this.driver=driver;
	}
	  public void employee_ill_form() throws InterruptedException  {
		  
		  driver.findElement(By.cssSelector("button[onclick^='javascript: selectUser_Window']")).click();
	        WebDriverWait  block = new WebDriverWait(driver,10);
	 	    WebElement select = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='selectuser-table']/tbody/tr[3]/td[1]/center/input[4]")));
	 	    select.click();
		    WebElement button = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='reassignDiv']/form/div/div/div[2]/button")));
		    button.click();
		   driver.findElement(By.cssSelector(".fa-folder-o")).click();
		    WebElement indept = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='1171-label']")));
		    indept.click();
		    driver.findElement(By.xpath("//span[contains(.,\'OK\')]")).click();
		    
		    driver.findElement(By.id("DateOfIncident")).click();
	        driver.findElement(By.cssSelector(".today")).click();
		    driver.findElement(By.id("NotificationDate")).click();
		    driver.findElement(By.cssSelector(".today")).click();
		    driver.findElement(By.cssSelector(".input-group-addon:nth-child(4)")).click();
		    driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).click();
		    driver.findElement(By.cssSelector(".bootstrap-timepicker-hour")).sendKeys("4");
		    driver.findElement(By.id("Reason")).click();
		    driver.findElement(By.id("Reason")).sendKeys("wdqwqw");
		    driver.findElement(By.id("AbscenceDetails")).click();
		    driver.findElement(By.id("AbscenceDetails")).sendKeys("dwdwd");
		    driver.findElement(By.id("CallReceivedBy")).click();
	        driver.findElement(By.id("CallReceivedBy")).sendKeys("fdf");
		    driver.findElement(By.cssSelector(".btn-info")).click();
		    Thread.sleep(4000);
}
}
