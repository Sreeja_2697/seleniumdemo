package employee_ill;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AttachEmIll {
	
	WebDriver driver;
	
	public AttachEmIll(WebDriver driver){
		this.driver=driver;
	}
	 public void employee_attachment_save() throws InterruptedException{
	    	
    	 WebElement attach=driver.findElement(By.linkText("Attachments"));
    		  attach.click();
    		  WebDriverWait  block = new WebDriverWait(driver,10);
      	    WebElement button = block.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[2]/div/button")));
      	    button.click();
      	    WebElement attach1 = block.until(ExpectedConditions.visibilityOfElementLocated(By.id("createAttachmentForm")));
      	    attach1.findElement(By.id("Title")).click();
      	    attach1.findElement(By.id("Title")).sendKeys("ttt");
      	    WebElement upload = attach.findElement(By.xpath("//input[@id='file']"));
      	    upload.sendKeys("C:\\BrowserDrivers\\Screenshot (1).png");
      	    driver.findElement(By.id("Upload")).click();
        Thread.sleep(4000);
    	
    }

}
